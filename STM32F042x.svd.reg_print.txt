\ STM32F042x Register Print file for Mecrisp-Stellaris Forth by Matthias Koch
\ By Terry Porter "terry@tjporter.com.au"

: CRC. cr
." CRC_DR: " CRC_DR @ hex. cr
." CRC_IDR: " CRC_IDR @ hex. cr
." CRC_CR: " CRC_CR @ hex. cr
." CRC_INIT: " CRC_INIT @ hex. cr
;

: GPIOF. cr
." GPIOF_MODER: " GPIOF_MODER @ hex. cr
." GPIOF_OTYPER: " GPIOF_OTYPER @ hex. cr
." GPIOF_OSPEEDR: " GPIOF_OSPEEDR @ hex. cr
." GPIOF_PUPDR: " GPIOF_PUPDR @ hex. cr
." GPIOF_IDR: " GPIOF_IDR @ hex. cr
." GPIOF_ODR: " GPIOF_ODR @ hex. cr
." GPIOF_BSRR: " GPIOF_BSRR @ hex. cr
." GPIOF_LCKR: " GPIOF_LCKR @ hex. cr
." GPIOF_AFRL: " GPIOF_AFRL @ hex. cr
." GPIOF_AFRH: " GPIOF_AFRH @ hex. cr
." GPIOF_BRR: " GPIOF_BRR @ hex. cr
;

: GPIOC. cr
;

: GPIOB. cr
;

: GPIOA. cr
." GPIOA_MODER: " GPIOA_MODER @ hex. cr
." GPIOA_OTYPER: " GPIOA_OTYPER @ hex. cr
." GPIOA_OSPEEDR: " GPIOA_OSPEEDR @ hex. cr
." GPIOA_PUPDR: " GPIOA_PUPDR @ hex. cr
." GPIOA_IDR: " GPIOA_IDR @ hex. cr
." GPIOA_ODR: " GPIOA_ODR @ hex. cr
." GPIOA_BSRR: " GPIOA_BSRR @ hex. cr
." GPIOA_LCKR: " GPIOA_LCKR @ hex. cr
." GPIOA_AFRL: " GPIOA_AFRL @ hex. cr
." GPIOA_AFRH: " GPIOA_AFRH @ hex. cr
." GPIOA_BRR: " GPIOA_BRR @ hex. cr
;

: SPI1. cr
." SPI1_CR1: " SPI1_CR1 @ hex. cr
." SPI1_CR2: " SPI1_CR2 @ hex. cr
." SPI1_SR: " SPI1_SR @ hex. cr
." SPI1_DR: " SPI1_DR @ hex. cr
." SPI1_CRCPR: " SPI1_CRCPR @ hex. cr
." SPI1_RXCRCR: " SPI1_RXCRCR @ hex. cr
." SPI1_TXCRCR: " SPI1_TXCRCR @ hex. cr
." SPI1_I2SCFGR: " SPI1_I2SCFGR @ hex. cr
." SPI1_I2SPR: " SPI1_I2SPR @ hex. cr
;

: SPI2. cr
;

: PWR. cr
." PWR_CR: " PWR_CR @ hex. cr
." PWR_CSR: " PWR_CSR @ hex. cr
;

: I2C1. cr
." I2C1_CR1: " I2C1_CR1 @ hex. cr
." I2C1_CR2: " I2C1_CR2 @ hex. cr
." I2C1_OAR1: " I2C1_OAR1 @ hex. cr
." I2C1_OAR2: " I2C1_OAR2 @ hex. cr
." I2C1_TIMINGR: " I2C1_TIMINGR @ hex. cr
." I2C1_TIMEOUTR: " I2C1_TIMEOUTR @ hex. cr
." I2C1_ISR: " I2C1_ISR @ hex. cr
." I2C1_ICR: " I2C1_ICR @ hex. cr
." I2C1_PECR: " I2C1_PECR @ hex. cr
." I2C1_RXDR: " I2C1_RXDR @ hex. cr
." I2C1_TXDR: " I2C1_TXDR @ hex. cr
;

: IWDG. cr
." IWDG_KR: " IWDG_KR @ hex. cr
." IWDG_PR: " IWDG_PR @ hex. cr
." IWDG_RLR: " IWDG_RLR @ hex. cr
." IWDG_SR: " IWDG_SR @ hex. cr
." IWDG_WINR: " IWDG_WINR @ hex. cr
;

: WWDG. cr
." WWDG_CR: " WWDG_CR @ hex. cr
." WWDG_CFR: " WWDG_CFR @ hex. cr
." WWDG_SR: " WWDG_SR @ hex. cr
;

: TIM1. cr
." TIM1_CR1: " TIM1_CR1 @ hex. cr
." TIM1_CR2: " TIM1_CR2 @ hex. cr
." TIM1_SMCR: " TIM1_SMCR @ hex. cr
." TIM1_DIER: " TIM1_DIER @ hex. cr
." TIM1_SR: " TIM1_SR @ hex. cr
." TIM1_EGR: " TIM1_EGR @ hex. cr
." TIM1_CCMR1_Output: " TIM1_CCMR1_Output @ hex. cr
." TIM1_CCMR1_Input: " TIM1_CCMR1_Input @ hex. cr
." TIM1_CCMR2_Output: " TIM1_CCMR2_Output @ hex. cr
." TIM1_CCMR2_Input: " TIM1_CCMR2_Input @ hex. cr
." TIM1_CCER: " TIM1_CCER @ hex. cr
." TIM1_CNT: " TIM1_CNT @ hex. cr
." TIM1_PSC: " TIM1_PSC @ hex. cr
." TIM1_ARR: " TIM1_ARR @ hex. cr
." TIM1_RCR: " TIM1_RCR @ hex. cr
." TIM1_CCR1: " TIM1_CCR1 @ hex. cr
." TIM1_CCR2: " TIM1_CCR2 @ hex. cr
." TIM1_CCR3: " TIM1_CCR3 @ hex. cr
." TIM1_CCR4: " TIM1_CCR4 @ hex. cr
." TIM1_BDTR: " TIM1_BDTR @ hex. cr
." TIM1_DCR: " TIM1_DCR @ hex. cr
." TIM1_DMAR: " TIM1_DMAR @ hex. cr
;

: TIM2. cr
." TIM2_CR1: " TIM2_CR1 @ hex. cr
." TIM2_CR2: " TIM2_CR2 @ hex. cr
." TIM2_SMCR: " TIM2_SMCR @ hex. cr
." TIM2_DIER: " TIM2_DIER @ hex. cr
." TIM2_SR: " TIM2_SR @ hex. cr
." TIM2_EGR: " TIM2_EGR @ hex. cr
." TIM2_CCMR1_Output: " TIM2_CCMR1_Output @ hex. cr
." TIM2_CCMR1_Input: " TIM2_CCMR1_Input @ hex. cr
." TIM2_CCMR2_Output: " TIM2_CCMR2_Output @ hex. cr
." TIM2_CCMR2_Input: " TIM2_CCMR2_Input @ hex. cr
." TIM2_CCER: " TIM2_CCER @ hex. cr
." TIM2_CNT: " TIM2_CNT @ hex. cr
." TIM2_PSC: " TIM2_PSC @ hex. cr
." TIM2_ARR: " TIM2_ARR @ hex. cr
." TIM2_CCR1: " TIM2_CCR1 @ hex. cr
." TIM2_CCR2: " TIM2_CCR2 @ hex. cr
." TIM2_CCR3: " TIM2_CCR3 @ hex. cr
." TIM2_CCR4: " TIM2_CCR4 @ hex. cr
." TIM2_DCR: " TIM2_DCR @ hex. cr
." TIM2_DMAR: " TIM2_DMAR @ hex. cr
;

: TIM3. cr
;

: TIM14. cr
." TIM14_CR1: " TIM14_CR1 @ hex. cr
." TIM14_DIER: " TIM14_DIER @ hex. cr
." TIM14_SR: " TIM14_SR @ hex. cr
." TIM14_EGR: " TIM14_EGR @ hex. cr
." TIM14_CCMR1_Output: " TIM14_CCMR1_Output @ hex. cr
." TIM14_CCMR1_Input: " TIM14_CCMR1_Input @ hex. cr
." TIM14_CCER: " TIM14_CCER @ hex. cr
." TIM14_CNT: " TIM14_CNT @ hex. cr
." TIM14_PSC: " TIM14_PSC @ hex. cr
." TIM14_ARR: " TIM14_ARR @ hex. cr
." TIM14_CCR1: " TIM14_CCR1 @ hex. cr
." TIM14_OR: " TIM14_OR @ hex. cr
;

: EXTI. cr
." EXTI_IMR: " EXTI_IMR @ hex. cr
." EXTI_EMR: " EXTI_EMR @ hex. cr
." EXTI_RTSR: " EXTI_RTSR @ hex. cr
." EXTI_FTSR: " EXTI_FTSR @ hex. cr
." EXTI_SWIER: " EXTI_SWIER @ hex. cr
." EXTI_PR: " EXTI_PR @ hex. cr
;

: NVIC. cr
." NVIC_ISER: " NVIC_ISER @ hex. cr
." NVIC_ICER: " NVIC_ICER @ hex. cr
." NVIC_ISPR: " NVIC_ISPR @ hex. cr
." NVIC_ICPR: " NVIC_ICPR @ hex. cr
." NVIC_IPR0: " NVIC_IPR0 @ hex. cr
." NVIC_IPR1: " NVIC_IPR1 @ hex. cr
." NVIC_IPR2: " NVIC_IPR2 @ hex. cr
." NVIC_IPR3: " NVIC_IPR3 @ hex. cr
." NVIC_IPR4: " NVIC_IPR4 @ hex. cr
." NVIC_IPR5: " NVIC_IPR5 @ hex. cr
." NVIC_IPR6: " NVIC_IPR6 @ hex. cr
." NVIC_IPR7: " NVIC_IPR7 @ hex. cr
;

: DMA. cr
." DMA_ISR: " DMA_ISR @ hex. cr
." DMA_IFCR: " DMA_IFCR @ hex. cr
." DMA_CCR1: " DMA_CCR1 @ hex. cr
." DMA_CNDTR1: " DMA_CNDTR1 @ hex. cr
." DMA_CPAR1: " DMA_CPAR1 @ hex. cr
." DMA_CMAR1: " DMA_CMAR1 @ hex. cr
." DMA_CCR2: " DMA_CCR2 @ hex. cr
." DMA_CNDTR2: " DMA_CNDTR2 @ hex. cr
." DMA_CPAR2: " DMA_CPAR2 @ hex. cr
." DMA_CMAR2: " DMA_CMAR2 @ hex. cr
." DMA_CCR3: " DMA_CCR3 @ hex. cr
." DMA_CNDTR3: " DMA_CNDTR3 @ hex. cr
." DMA_CPAR3: " DMA_CPAR3 @ hex. cr
." DMA_CMAR3: " DMA_CMAR3 @ hex. cr
." DMA_CCR4: " DMA_CCR4 @ hex. cr
." DMA_CNDTR4: " DMA_CNDTR4 @ hex. cr
." DMA_CPAR4: " DMA_CPAR4 @ hex. cr
." DMA_CMAR4: " DMA_CMAR4 @ hex. cr
." DMA_CCR5: " DMA_CCR5 @ hex. cr
." DMA_CNDTR5: " DMA_CNDTR5 @ hex. cr
." DMA_CPAR5: " DMA_CPAR5 @ hex. cr
." DMA_CMAR5: " DMA_CMAR5 @ hex. cr
." DMA_CCR6: " DMA_CCR6 @ hex. cr
." DMA_CNDTR6: " DMA_CNDTR6 @ hex. cr
." DMA_CPAR6: " DMA_CPAR6 @ hex. cr
." DMA_CMAR6: " DMA_CMAR6 @ hex. cr
." DMA_CCR7: " DMA_CCR7 @ hex. cr
." DMA_CNDTR7: " DMA_CNDTR7 @ hex. cr
." DMA_CPAR7: " DMA_CPAR7 @ hex. cr
." DMA_CMAR7: " DMA_CMAR7 @ hex. cr
;

: RCC. cr
." RCC_CR: " RCC_CR @ hex. cr
." RCC_CFGR: " RCC_CFGR @ hex. cr
." RCC_CIR: " RCC_CIR @ hex. cr
." RCC_APB2RSTR: " RCC_APB2RSTR @ hex. cr
." RCC_APB1RSTR: " RCC_APB1RSTR @ hex. cr
." RCC_AHBENR: " RCC_AHBENR @ hex. cr
." RCC_APB2ENR: " RCC_APB2ENR @ hex. cr
." RCC_APB1ENR: " RCC_APB1ENR @ hex. cr
." RCC_BDCR: " RCC_BDCR @ hex. cr
." RCC_CSR: " RCC_CSR @ hex. cr
." RCC_AHBRSTR: " RCC_AHBRSTR @ hex. cr
." RCC_CFGR2: " RCC_CFGR2 @ hex. cr
." RCC_CFGR3: " RCC_CFGR3 @ hex. cr
." RCC_CR2: " RCC_CR2 @ hex. cr
;

: SYSCFG. cr
." SYSCFG_CFGR1: " SYSCFG_CFGR1 @ hex. cr
." SYSCFG_EXTICR1: " SYSCFG_EXTICR1 @ hex. cr
." SYSCFG_EXTICR2: " SYSCFG_EXTICR2 @ hex. cr
." SYSCFG_EXTICR3: " SYSCFG_EXTICR3 @ hex. cr
." SYSCFG_EXTICR4: " SYSCFG_EXTICR4 @ hex. cr
." SYSCFG_CFGR2: " SYSCFG_CFGR2 @ hex. cr
;

: ADC. cr
." ADC_ISR: " ADC_ISR @ hex. cr
." ADC_IER: " ADC_IER @ hex. cr
." ADC_CR: " ADC_CR @ hex. cr
." ADC_CFGR1: " ADC_CFGR1 @ hex. cr
." ADC_CFGR2: " ADC_CFGR2 @ hex. cr
." ADC_SMPR: " ADC_SMPR @ hex. cr
." ADC_TR: " ADC_TR @ hex. cr
." ADC_CHSELR: " ADC_CHSELR @ hex. cr
." ADC_DR: " ADC_DR @ hex. cr
." ADC_CCR: " ADC_CCR @ hex. cr
;

: USART1. cr
." USART1_CR1: " USART1_CR1 @ hex. cr
." USART1_CR2: " USART1_CR2 @ hex. cr
." USART1_CR3: " USART1_CR3 @ hex. cr
." USART1_BRR: " USART1_BRR @ hex. cr
." USART1_GTPR: " USART1_GTPR @ hex. cr
." USART1_RTOR: " USART1_RTOR @ hex. cr
." USART1_RQR: " USART1_RQR @ hex. cr
." USART1_ISR: " USART1_ISR @ hex. cr
." USART1_ICR: " USART1_ICR @ hex. cr
." USART1_RDR: " USART1_RDR @ hex. cr
." USART1_TDR: " USART1_TDR @ hex. cr
;

: USART2. cr
;

: COMP. cr
." COMP_CSR: " COMP_CSR @ hex. cr
;

: RTC. cr
." RTC_TR: " RTC_TR @ hex. cr
." RTC_DR: " RTC_DR @ hex. cr
." RTC_CR: " RTC_CR @ hex. cr
." RTC_ISR: " RTC_ISR @ hex. cr
." RTC_PRER: " RTC_PRER @ hex. cr
." RTC_ALRMAR: " RTC_ALRMAR @ hex. cr
." RTC_WPR: " RTC_WPR @ hex. cr
." RTC_SSR: " RTC_SSR @ hex. cr
." RTC_SHIFTR: " RTC_SHIFTR @ hex. cr
." RTC_TSTR: " RTC_TSTR @ hex. cr
." RTC_TSDR: " RTC_TSDR @ hex. cr
." RTC_TSSSR: " RTC_TSSSR @ hex. cr
." RTC_CALR: " RTC_CALR @ hex. cr
." RTC_TAFCR: " RTC_TAFCR @ hex. cr
." RTC_ALRMASSR: " RTC_ALRMASSR @ hex. cr
." RTC_BKP0R: " RTC_BKP0R @ hex. cr
." RTC_BKP1R: " RTC_BKP1R @ hex. cr
." RTC_BKP2R: " RTC_BKP2R @ hex. cr
." RTC_BKP3R: " RTC_BKP3R @ hex. cr
." RTC_BKP4R: " RTC_BKP4R @ hex. cr
;

: TIM16. cr
." TIM16_CR1: " TIM16_CR1 @ hex. cr
." TIM16_CR2: " TIM16_CR2 @ hex. cr
." TIM16_DIER: " TIM16_DIER @ hex. cr
." TIM16_SR: " TIM16_SR @ hex. cr
." TIM16_EGR: " TIM16_EGR @ hex. cr
." TIM16_CCMR1_Output: " TIM16_CCMR1_Output @ hex. cr
." TIM16_CCMR1_Input: " TIM16_CCMR1_Input @ hex. cr
." TIM16_CCER: " TIM16_CCER @ hex. cr
." TIM16_CNT: " TIM16_CNT @ hex. cr
." TIM16_PSC: " TIM16_PSC @ hex. cr
." TIM16_ARR: " TIM16_ARR @ hex. cr
." TIM16_RCR: " TIM16_RCR @ hex. cr
." TIM16_CCR1: " TIM16_CCR1 @ hex. cr
." TIM16_BDTR: " TIM16_BDTR @ hex. cr
." TIM16_DCR: " TIM16_DCR @ hex. cr
." TIM16_DMAR: " TIM16_DMAR @ hex. cr
;

: TIM17. cr
;

: TSC. cr
." TSC_CR: " TSC_CR @ hex. cr
." TSC_IER: " TSC_IER @ hex. cr
." TSC_ICR: " TSC_ICR @ hex. cr
." TSC_ISR: " TSC_ISR @ hex. cr
." TSC_IOHCR: " TSC_IOHCR @ hex. cr
." TSC_IOASCR: " TSC_IOASCR @ hex. cr
." TSC_IOSCR: " TSC_IOSCR @ hex. cr
." TSC_IOCCR: " TSC_IOCCR @ hex. cr
." TSC_IOGCSR: " TSC_IOGCSR @ hex. cr
." TSC_IOG1CR: " TSC_IOG1CR @ hex. cr
." TSC_IOG2CR: " TSC_IOG2CR @ hex. cr
." TSC_IOG3CR: " TSC_IOG3CR @ hex. cr
." TSC_IOG4CR: " TSC_IOG4CR @ hex. cr
." TSC_IOG5CR: " TSC_IOG5CR @ hex. cr
." TSC_IOG6CR: " TSC_IOG6CR @ hex. cr
;

: CEC. cr
." CEC_CR: " CEC_CR @ hex. cr
." CEC_CFGR: " CEC_CFGR @ hex. cr
." CEC_TXDR: " CEC_TXDR @ hex. cr
." CEC_RXDR: " CEC_RXDR @ hex. cr
." CEC_ISR: " CEC_ISR @ hex. cr
." CEC_IER: " CEC_IER @ hex. cr
;

: Flash. cr
." Flash_ACR: " Flash_ACR @ hex. cr
." Flash_KEYR: " Flash_KEYR @ hex. cr
." Flash_OPTKEYR: " Flash_OPTKEYR @ hex. cr
." Flash_SR: " Flash_SR @ hex. cr
." Flash_CR: " Flash_CR @ hex. cr
." Flash_AR: " Flash_AR @ hex. cr
." Flash_OBR: " Flash_OBR @ hex. cr
." Flash_WRPR: " Flash_WRPR @ hex. cr
;

: DBGMCU. cr
." DBGMCU_IDCODE: " DBGMCU_IDCODE @ hex. cr
." DBGMCU_CR: " DBGMCU_CR @ hex. cr
." DBGMCU_APBLFZ: " DBGMCU_APBLFZ @ hex. cr
." DBGMCU_APBHFZ: " DBGMCU_APBHFZ @ hex. cr
;

: USB. cr
." USB_EP0R: " USB_EP0R @ hex. cr
." USB_EP1R: " USB_EP1R @ hex. cr
." USB_EP2R: " USB_EP2R @ hex. cr
." USB_EP3R: " USB_EP3R @ hex. cr
." USB_EP4R: " USB_EP4R @ hex. cr
." USB_EP5R: " USB_EP5R @ hex. cr
." USB_EP6R: " USB_EP6R @ hex. cr
." USB_EP7R: " USB_EP7R @ hex. cr
." USB_CNTR: " USB_CNTR @ hex. cr
." USB_ISTR: " USB_ISTR @ hex. cr
." USB_FNR: " USB_FNR @ hex. cr
." USB_DADDR: " USB_DADDR @ hex. cr
." USB_BTABLE: " USB_BTABLE @ hex. cr
." USB_LPMCSR: " USB_LPMCSR @ hex. cr
." USB_BCDR: " USB_BCDR @ hex. cr
;

: CRS. cr
." CRS_CR: " CRS_CR @ hex. cr
." CRS_CFGR: " CRS_CFGR @ hex. cr
." CRS_ISR: " CRS_ISR @ hex. cr
." CRS_ICR: " CRS_ICR @ hex. cr
;

: CAN. cr
." CAN_CAN_MCR: " CAN_CAN_MCR @ hex. cr
." CAN_CAN_MSR: " CAN_CAN_MSR @ hex. cr
." CAN_CAN_TSR: " CAN_CAN_TSR @ hex. cr
." CAN_CAN_RF0R: " CAN_CAN_RF0R @ hex. cr
." CAN_CAN_RF1R: " CAN_CAN_RF1R @ hex. cr
." CAN_CAN_IER: " CAN_CAN_IER @ hex. cr
." CAN_CAN_ESR: " CAN_CAN_ESR @ hex. cr
." CAN_CAN_BTR: " CAN_CAN_BTR @ hex. cr
." CAN_CAN_TI0R: " CAN_CAN_TI0R @ hex. cr
." CAN_CAN_TDT0R: " CAN_CAN_TDT0R @ hex. cr
." CAN_CAN_TDL0R: " CAN_CAN_TDL0R @ hex. cr
." CAN_CAN_TDH0R: " CAN_CAN_TDH0R @ hex. cr
." CAN_CAN_TI1R: " CAN_CAN_TI1R @ hex. cr
." CAN_CAN_TDT1R: " CAN_CAN_TDT1R @ hex. cr
." CAN_CAN_TDL1R: " CAN_CAN_TDL1R @ hex. cr
." CAN_CAN_TDH1R: " CAN_CAN_TDH1R @ hex. cr
." CAN_CAN_TI2R: " CAN_CAN_TI2R @ hex. cr
." CAN_CAN_TDT2R: " CAN_CAN_TDT2R @ hex. cr
." CAN_CAN_TDL2R: " CAN_CAN_TDL2R @ hex. cr
." CAN_CAN_TDH2R: " CAN_CAN_TDH2R @ hex. cr
." CAN_CAN_RI0R: " CAN_CAN_RI0R @ hex. cr
." CAN_CAN_RDT0R: " CAN_CAN_RDT0R @ hex. cr
." CAN_CAN_RDL0R: " CAN_CAN_RDL0R @ hex. cr
." CAN_CAN_RDH0R: " CAN_CAN_RDH0R @ hex. cr
." CAN_CAN_RI1R: " CAN_CAN_RI1R @ hex. cr
." CAN_CAN_RDT1R: " CAN_CAN_RDT1R @ hex. cr
." CAN_CAN_RDL1R: " CAN_CAN_RDL1R @ hex. cr
." CAN_CAN_RDH1R: " CAN_CAN_RDH1R @ hex. cr
." CAN_CAN_FMR: " CAN_CAN_FMR @ hex. cr
." CAN_CAN_FM1R: " CAN_CAN_FM1R @ hex. cr
." CAN_CAN_FS1R: " CAN_CAN_FS1R @ hex. cr
." CAN_CAN_FFA1R: " CAN_CAN_FFA1R @ hex. cr
." CAN_CAN_FA1R: " CAN_CAN_FA1R @ hex. cr
." CAN_F0R1: " CAN_F0R1 @ hex. cr
." CAN_F0R2: " CAN_F0R2 @ hex. cr
." CAN_F1R1: " CAN_F1R1 @ hex. cr
." CAN_F1R2: " CAN_F1R2 @ hex. cr
." CAN_F2R1: " CAN_F2R1 @ hex. cr
." CAN_F2R2: " CAN_F2R2 @ hex. cr
." CAN_F3R1: " CAN_F3R1 @ hex. cr
." CAN_F3R2: " CAN_F3R2 @ hex. cr
." CAN_F4R1: " CAN_F4R1 @ hex. cr
." CAN_F4R2: " CAN_F4R2 @ hex. cr
." CAN_F5R1: " CAN_F5R1 @ hex. cr
." CAN_F5R2: " CAN_F5R2 @ hex. cr
." CAN_F6R1: " CAN_F6R1 @ hex. cr
." CAN_F6R2: " CAN_F6R2 @ hex. cr
." CAN_F7R1: " CAN_F7R1 @ hex. cr
." CAN_F7R2: " CAN_F7R2 @ hex. cr
." CAN_F8R1: " CAN_F8R1 @ hex. cr
." CAN_F8R2: " CAN_F8R2 @ hex. cr
." CAN_F9R1: " CAN_F9R1 @ hex. cr
." CAN_F9R2: " CAN_F9R2 @ hex. cr
." CAN_F10R1: " CAN_F10R1 @ hex. cr
." CAN_F10R2: " CAN_F10R2 @ hex. cr
." CAN_F11R1: " CAN_F11R1 @ hex. cr
." CAN_F11R2: " CAN_F11R2 @ hex. cr
." CAN_F12R1: " CAN_F12R1 @ hex. cr
." CAN_F12R2: " CAN_F12R2 @ hex. cr
." CAN_F13R1: " CAN_F13R1 @ hex. cr
." CAN_F13R2: " CAN_F13R2 @ hex. cr
." CAN_F14R1: " CAN_F14R1 @ hex. cr
." CAN_F14R2: " CAN_F14R2 @ hex. cr
." CAN_F15R1: " CAN_F15R1 @ hex. cr
." CAN_F15R2: " CAN_F15R2 @ hex. cr
." CAN_F16R1: " CAN_F16R1 @ hex. cr
." CAN_F16R2: " CAN_F16R2 @ hex. cr
." CAN_F17R1: " CAN_F17R1 @ hex. cr
." CAN_F17R2: " CAN_F17R2 @ hex. cr
." CAN_F18R1: " CAN_F18R1 @ hex. cr
." CAN_F18R2: " CAN_F18R2 @ hex. cr
." CAN_F19R1: " CAN_F19R1 @ hex. cr
." CAN_F19R2: " CAN_F19R2 @ hex. cr
." CAN_F20R1: " CAN_F20R1 @ hex. cr
." CAN_F20R2: " CAN_F20R2 @ hex. cr
." CAN_F21R1: " CAN_F21R1 @ hex. cr
." CAN_F21R2: " CAN_F21R2 @ hex. cr
." CAN_F22R1: " CAN_F22R1 @ hex. cr
." CAN_F22R2: " CAN_F22R2 @ hex. cr
." CAN_F23R1: " CAN_F23R1 @ hex. cr
." CAN_F23R2: " CAN_F23R2 @ hex. cr
." CAN_F24R1: " CAN_F24R1 @ hex. cr
." CAN_F24R2: " CAN_F24R2 @ hex. cr
." CAN_F25R1: " CAN_F25R1 @ hex. cr
." CAN_F25R2: " CAN_F25R2 @ hex. cr
." CAN_F26R1: " CAN_F26R1 @ hex. cr
." CAN_F26R2: " CAN_F26R2 @ hex. cr
." CAN_F27R1: " CAN_F27R1 @ hex. cr
." CAN_F27R2: " CAN_F27R2 @ hex. cr
;


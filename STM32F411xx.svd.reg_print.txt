\ STM32F411xx Register Print file for Mecrisp-Stellaris Forth by Matthias Koch
\ By Terry Porter "terry@tjporter.com.au"

: ADC_Common. cr
." ADC_Common_CSR: " ADC_Common_CSR @ hex. cr
." ADC_Common_CCR: " ADC_Common_CCR @ hex. cr
;

: ADC1. cr
." ADC1_SR: " ADC1_SR @ hex. cr
." ADC1_CR1: " ADC1_CR1 @ hex. cr
." ADC1_CR2: " ADC1_CR2 @ hex. cr
." ADC1_SMPR1: " ADC1_SMPR1 @ hex. cr
." ADC1_SMPR2: " ADC1_SMPR2 @ hex. cr
." ADC1_JOFR1: " ADC1_JOFR1 @ hex. cr
." ADC1_JOFR2: " ADC1_JOFR2 @ hex. cr
." ADC1_JOFR3: " ADC1_JOFR3 @ hex. cr
." ADC1_JOFR4: " ADC1_JOFR4 @ hex. cr
." ADC1_HTR: " ADC1_HTR @ hex. cr
." ADC1_LTR: " ADC1_LTR @ hex. cr
." ADC1_SQR1: " ADC1_SQR1 @ hex. cr
." ADC1_SQR2: " ADC1_SQR2 @ hex. cr
." ADC1_SQR3: " ADC1_SQR3 @ hex. cr
." ADC1_JSQR: " ADC1_JSQR @ hex. cr
." ADC1_JDR1: " ADC1_JDR1 @ hex. cr
." ADC1_JDR2: " ADC1_JDR2 @ hex. cr
." ADC1_JDR3: " ADC1_JDR3 @ hex. cr
." ADC1_JDR4: " ADC1_JDR4 @ hex. cr
." ADC1_DR: " ADC1_DR @ hex. cr
;

: CRC. cr
." CRC_DR: " CRC_DR @ hex. cr
." CRC_IDR: " CRC_IDR @ hex. cr
." CRC_CR: " CRC_CR @ hex. cr
;

: DBG. cr
." DBG_DBGMCU_IDCODE: " DBG_DBGMCU_IDCODE @ hex. cr
." DBG_DBGMCU_CR: " DBG_DBGMCU_CR @ hex. cr
." DBG_DBGMCU_APB1_FZ: " DBG_DBGMCU_APB1_FZ @ hex. cr
." DBG_DBGMCU_APB2_FZ: " DBG_DBGMCU_APB2_FZ @ hex. cr
;

: EXTI. cr
." EXTI_IMR: " EXTI_IMR @ hex. cr
." EXTI_EMR: " EXTI_EMR @ hex. cr
." EXTI_RTSR: " EXTI_RTSR @ hex. cr
." EXTI_FTSR: " EXTI_FTSR @ hex. cr
." EXTI_SWIER: " EXTI_SWIER @ hex. cr
." EXTI_PR: " EXTI_PR @ hex. cr
;

: FLASH. cr
." FLASH_ACR: " FLASH_ACR @ hex. cr
." FLASH_KEYR: " FLASH_KEYR @ hex. cr
." FLASH_OPTKEYR: " FLASH_OPTKEYR @ hex. cr
." FLASH_SR: " FLASH_SR @ hex. cr
." FLASH_CR: " FLASH_CR @ hex. cr
." FLASH_OPTCR: " FLASH_OPTCR @ hex. cr
;

: IWDG. cr
." IWDG_KR: " IWDG_KR @ hex. cr
." IWDG_PR: " IWDG_PR @ hex. cr
." IWDG_RLR: " IWDG_RLR @ hex. cr
." IWDG_SR: " IWDG_SR @ hex. cr
;

: OTG_FS_DEVICE. cr
." OTG_FS_DEVICE_FS_DCFG: " OTG_FS_DEVICE_FS_DCFG @ hex. cr
." OTG_FS_DEVICE_FS_DCTL: " OTG_FS_DEVICE_FS_DCTL @ hex. cr
." OTG_FS_DEVICE_FS_DSTS: " OTG_FS_DEVICE_FS_DSTS @ hex. cr
." OTG_FS_DEVICE_FS_DIEPMSK: " OTG_FS_DEVICE_FS_DIEPMSK @ hex. cr
." OTG_FS_DEVICE_FS_DOEPMSK: " OTG_FS_DEVICE_FS_DOEPMSK @ hex. cr
." OTG_FS_DEVICE_FS_DAINT: " OTG_FS_DEVICE_FS_DAINT @ hex. cr
." OTG_FS_DEVICE_FS_DAINTMSK: " OTG_FS_DEVICE_FS_DAINTMSK @ hex. cr
." OTG_FS_DEVICE_DVBUSDIS: " OTG_FS_DEVICE_DVBUSDIS @ hex. cr
." OTG_FS_DEVICE_DVBUSPULSE: " OTG_FS_DEVICE_DVBUSPULSE @ hex. cr
." OTG_FS_DEVICE_DIEPEMPMSK: " OTG_FS_DEVICE_DIEPEMPMSK @ hex. cr
." OTG_FS_DEVICE_FS_DIEPCTL0: " OTG_FS_DEVICE_FS_DIEPCTL0 @ hex. cr
." OTG_FS_DEVICE_DIEPCTL1: " OTG_FS_DEVICE_DIEPCTL1 @ hex. cr
." OTG_FS_DEVICE_DIEPCTL2: " OTG_FS_DEVICE_DIEPCTL2 @ hex. cr
." OTG_FS_DEVICE_DIEPCTL3: " OTG_FS_DEVICE_DIEPCTL3 @ hex. cr
." OTG_FS_DEVICE_DOEPCTL0: " OTG_FS_DEVICE_DOEPCTL0 @ hex. cr
." OTG_FS_DEVICE_DOEPCTL1: " OTG_FS_DEVICE_DOEPCTL1 @ hex. cr
." OTG_FS_DEVICE_DOEPCTL2: " OTG_FS_DEVICE_DOEPCTL2 @ hex. cr
." OTG_FS_DEVICE_DOEPCTL3: " OTG_FS_DEVICE_DOEPCTL3 @ hex. cr
." OTG_FS_DEVICE_DIEPINT0: " OTG_FS_DEVICE_DIEPINT0 @ hex. cr
." OTG_FS_DEVICE_DIEPINT1: " OTG_FS_DEVICE_DIEPINT1 @ hex. cr
." OTG_FS_DEVICE_DIEPINT2: " OTG_FS_DEVICE_DIEPINT2 @ hex. cr
." OTG_FS_DEVICE_DIEPINT3: " OTG_FS_DEVICE_DIEPINT3 @ hex. cr
." OTG_FS_DEVICE_DOEPINT0: " OTG_FS_DEVICE_DOEPINT0 @ hex. cr
." OTG_FS_DEVICE_DOEPINT1: " OTG_FS_DEVICE_DOEPINT1 @ hex. cr
." OTG_FS_DEVICE_DOEPINT2: " OTG_FS_DEVICE_DOEPINT2 @ hex. cr
." OTG_FS_DEVICE_DOEPINT3: " OTG_FS_DEVICE_DOEPINT3 @ hex. cr
." OTG_FS_DEVICE_DIEPTSIZ0: " OTG_FS_DEVICE_DIEPTSIZ0 @ hex. cr
." OTG_FS_DEVICE_DOEPTSIZ0: " OTG_FS_DEVICE_DOEPTSIZ0 @ hex. cr
." OTG_FS_DEVICE_DIEPTSIZ1: " OTG_FS_DEVICE_DIEPTSIZ1 @ hex. cr
." OTG_FS_DEVICE_DIEPTSIZ2: " OTG_FS_DEVICE_DIEPTSIZ2 @ hex. cr
." OTG_FS_DEVICE_DIEPTSIZ3: " OTG_FS_DEVICE_DIEPTSIZ3 @ hex. cr
." OTG_FS_DEVICE_DTXFSTS0: " OTG_FS_DEVICE_DTXFSTS0 @ hex. cr
." OTG_FS_DEVICE_DTXFSTS1: " OTG_FS_DEVICE_DTXFSTS1 @ hex. cr
." OTG_FS_DEVICE_DTXFSTS2: " OTG_FS_DEVICE_DTXFSTS2 @ hex. cr
." OTG_FS_DEVICE_DTXFSTS3: " OTG_FS_DEVICE_DTXFSTS3 @ hex. cr
." OTG_FS_DEVICE_DOEPTSIZ1: " OTG_FS_DEVICE_DOEPTSIZ1 @ hex. cr
." OTG_FS_DEVICE_DOEPTSIZ2: " OTG_FS_DEVICE_DOEPTSIZ2 @ hex. cr
." OTG_FS_DEVICE_DOEPTSIZ3: " OTG_FS_DEVICE_DOEPTSIZ3 @ hex. cr
;

: OTG_FS_GLOBAL. cr
." OTG_FS_GLOBAL_FS_GOTGCTL: " OTG_FS_GLOBAL_FS_GOTGCTL @ hex. cr
." OTG_FS_GLOBAL_FS_GOTGINT: " OTG_FS_GLOBAL_FS_GOTGINT @ hex. cr
." OTG_FS_GLOBAL_FS_GAHBCFG: " OTG_FS_GLOBAL_FS_GAHBCFG @ hex. cr
." OTG_FS_GLOBAL_FS_GUSBCFG: " OTG_FS_GLOBAL_FS_GUSBCFG @ hex. cr
." OTG_FS_GLOBAL_FS_GRSTCTL: " OTG_FS_GLOBAL_FS_GRSTCTL @ hex. cr
." OTG_FS_GLOBAL_FS_GINTSTS: " OTG_FS_GLOBAL_FS_GINTSTS @ hex. cr
." OTG_FS_GLOBAL_FS_GINTMSK: " OTG_FS_GLOBAL_FS_GINTMSK @ hex. cr
." OTG_FS_GLOBAL_FS_GRXSTSR_Device: " OTG_FS_GLOBAL_FS_GRXSTSR_Device @ hex. cr
." OTG_FS_GLOBAL_FS_GRXSTSR_Host: " OTG_FS_GLOBAL_FS_GRXSTSR_Host @ hex. cr
." OTG_FS_GLOBAL_FS_GRXFSIZ: " OTG_FS_GLOBAL_FS_GRXFSIZ @ hex. cr
." OTG_FS_GLOBAL_FS_GNPTXFSIZ_Device: " OTG_FS_GLOBAL_FS_GNPTXFSIZ_Device @ hex. cr
." OTG_FS_GLOBAL_FS_GNPTXFSIZ_Host: " OTG_FS_GLOBAL_FS_GNPTXFSIZ_Host @ hex. cr
." OTG_FS_GLOBAL_FS_GNPTXSTS: " OTG_FS_GLOBAL_FS_GNPTXSTS @ hex. cr
." OTG_FS_GLOBAL_FS_GCCFG: " OTG_FS_GLOBAL_FS_GCCFG @ hex. cr
." OTG_FS_GLOBAL_FS_CID: " OTG_FS_GLOBAL_FS_CID @ hex. cr
." OTG_FS_GLOBAL_FS_HPTXFSIZ: " OTG_FS_GLOBAL_FS_HPTXFSIZ @ hex. cr
." OTG_FS_GLOBAL_FS_DIEPTXF1: " OTG_FS_GLOBAL_FS_DIEPTXF1 @ hex. cr
." OTG_FS_GLOBAL_FS_DIEPTXF2: " OTG_FS_GLOBAL_FS_DIEPTXF2 @ hex. cr
." OTG_FS_GLOBAL_FS_DIEPTXF3: " OTG_FS_GLOBAL_FS_DIEPTXF3 @ hex. cr
;

: OTG_FS_HOST. cr
." OTG_FS_HOST_FS_HCFG: " OTG_FS_HOST_FS_HCFG @ hex. cr
." OTG_FS_HOST_HFIR: " OTG_FS_HOST_HFIR @ hex. cr
." OTG_FS_HOST_FS_HFNUM: " OTG_FS_HOST_FS_HFNUM @ hex. cr
." OTG_FS_HOST_FS_HPTXSTS: " OTG_FS_HOST_FS_HPTXSTS @ hex. cr
." OTG_FS_HOST_HAINT: " OTG_FS_HOST_HAINT @ hex. cr
." OTG_FS_HOST_HAINTMSK: " OTG_FS_HOST_HAINTMSK @ hex. cr
." OTG_FS_HOST_FS_HPRT: " OTG_FS_HOST_FS_HPRT @ hex. cr
." OTG_FS_HOST_FS_HCCHAR0: " OTG_FS_HOST_FS_HCCHAR0 @ hex. cr
." OTG_FS_HOST_FS_HCCHAR1: " OTG_FS_HOST_FS_HCCHAR1 @ hex. cr
." OTG_FS_HOST_FS_HCCHAR2: " OTG_FS_HOST_FS_HCCHAR2 @ hex. cr
." OTG_FS_HOST_FS_HCCHAR3: " OTG_FS_HOST_FS_HCCHAR3 @ hex. cr
." OTG_FS_HOST_FS_HCCHAR4: " OTG_FS_HOST_FS_HCCHAR4 @ hex. cr
." OTG_FS_HOST_FS_HCCHAR5: " OTG_FS_HOST_FS_HCCHAR5 @ hex. cr
." OTG_FS_HOST_FS_HCCHAR6: " OTG_FS_HOST_FS_HCCHAR6 @ hex. cr
." OTG_FS_HOST_FS_HCCHAR7: " OTG_FS_HOST_FS_HCCHAR7 @ hex. cr
." OTG_FS_HOST_FS_HCINT0: " OTG_FS_HOST_FS_HCINT0 @ hex. cr
." OTG_FS_HOST_FS_HCINT1: " OTG_FS_HOST_FS_HCINT1 @ hex. cr
." OTG_FS_HOST_FS_HCINT2: " OTG_FS_HOST_FS_HCINT2 @ hex. cr
." OTG_FS_HOST_FS_HCINT3: " OTG_FS_HOST_FS_HCINT3 @ hex. cr
." OTG_FS_HOST_FS_HCINT4: " OTG_FS_HOST_FS_HCINT4 @ hex. cr
." OTG_FS_HOST_FS_HCINT5: " OTG_FS_HOST_FS_HCINT5 @ hex. cr
." OTG_FS_HOST_FS_HCINT6: " OTG_FS_HOST_FS_HCINT6 @ hex. cr
." OTG_FS_HOST_FS_HCINT7: " OTG_FS_HOST_FS_HCINT7 @ hex. cr
." OTG_FS_HOST_FS_HCINTMSK0: " OTG_FS_HOST_FS_HCINTMSK0 @ hex. cr
." OTG_FS_HOST_FS_HCINTMSK1: " OTG_FS_HOST_FS_HCINTMSK1 @ hex. cr
." OTG_FS_HOST_FS_HCINTMSK2: " OTG_FS_HOST_FS_HCINTMSK2 @ hex. cr
." OTG_FS_HOST_FS_HCINTMSK3: " OTG_FS_HOST_FS_HCINTMSK3 @ hex. cr
." OTG_FS_HOST_FS_HCINTMSK4: " OTG_FS_HOST_FS_HCINTMSK4 @ hex. cr
." OTG_FS_HOST_FS_HCINTMSK5: " OTG_FS_HOST_FS_HCINTMSK5 @ hex. cr
." OTG_FS_HOST_FS_HCINTMSK6: " OTG_FS_HOST_FS_HCINTMSK6 @ hex. cr
." OTG_FS_HOST_FS_HCINTMSK7: " OTG_FS_HOST_FS_HCINTMSK7 @ hex. cr
." OTG_FS_HOST_FS_HCTSIZ0: " OTG_FS_HOST_FS_HCTSIZ0 @ hex. cr
." OTG_FS_HOST_FS_HCTSIZ1: " OTG_FS_HOST_FS_HCTSIZ1 @ hex. cr
." OTG_FS_HOST_FS_HCTSIZ2: " OTG_FS_HOST_FS_HCTSIZ2 @ hex. cr
." OTG_FS_HOST_FS_HCTSIZ3: " OTG_FS_HOST_FS_HCTSIZ3 @ hex. cr
." OTG_FS_HOST_FS_HCTSIZ4: " OTG_FS_HOST_FS_HCTSIZ4 @ hex. cr
." OTG_FS_HOST_FS_HCTSIZ5: " OTG_FS_HOST_FS_HCTSIZ5 @ hex. cr
." OTG_FS_HOST_FS_HCTSIZ6: " OTG_FS_HOST_FS_HCTSIZ6 @ hex. cr
." OTG_FS_HOST_FS_HCTSIZ7: " OTG_FS_HOST_FS_HCTSIZ7 @ hex. cr
;

: OTG_FS_PWRCLK. cr
." OTG_FS_PWRCLK_FS_PCGCCTL: " OTG_FS_PWRCLK_FS_PCGCCTL @ hex. cr
;

: PWR. cr
." PWR_CR: " PWR_CR @ hex. cr
." PWR_CSR: " PWR_CSR @ hex. cr
;

: RCC. cr
." RCC_CR: " RCC_CR @ hex. cr
." RCC_PLLCFGR: " RCC_PLLCFGR @ hex. cr
." RCC_CFGR: " RCC_CFGR @ hex. cr
." RCC_CIR: " RCC_CIR @ hex. cr
." RCC_AHB1RSTR: " RCC_AHB1RSTR @ hex. cr
." RCC_AHB2RSTR: " RCC_AHB2RSTR @ hex. cr
." RCC_APB1RSTR: " RCC_APB1RSTR @ hex. cr
." RCC_APB2RSTR: " RCC_APB2RSTR @ hex. cr
." RCC_AHB1ENR: " RCC_AHB1ENR @ hex. cr
." RCC_AHB2ENR: " RCC_AHB2ENR @ hex. cr
." RCC_APB1ENR: " RCC_APB1ENR @ hex. cr
." RCC_APB2ENR: " RCC_APB2ENR @ hex. cr
." RCC_AHB1LPENR: " RCC_AHB1LPENR @ hex. cr
." RCC_AHB2LPENR: " RCC_AHB2LPENR @ hex. cr
." RCC_APB1LPENR: " RCC_APB1LPENR @ hex. cr
." RCC_APB2LPENR: " RCC_APB2LPENR @ hex. cr
." RCC_BDCR: " RCC_BDCR @ hex. cr
." RCC_CSR: " RCC_CSR @ hex. cr
." RCC_SSCGR: " RCC_SSCGR @ hex. cr
." RCC_PLLI2SCFGR: " RCC_PLLI2SCFGR @ hex. cr
;

: RTC. cr
." RTC_TR: " RTC_TR @ hex. cr
." RTC_DR: " RTC_DR @ hex. cr
." RTC_CR: " RTC_CR @ hex. cr
." RTC_ISR: " RTC_ISR @ hex. cr
." RTC_PRER: " RTC_PRER @ hex. cr
." RTC_WUTR: " RTC_WUTR @ hex. cr
." RTC_CALIBR: " RTC_CALIBR @ hex. cr
." RTC_ALRMAR: " RTC_ALRMAR @ hex. cr
." RTC_ALRMBR: " RTC_ALRMBR @ hex. cr
." RTC_WPR: " RTC_WPR @ hex. cr
." RTC_SSR: " RTC_SSR @ hex. cr
." RTC_SHIFTR: " RTC_SHIFTR @ hex. cr
." RTC_TSTR: " RTC_TSTR @ hex. cr
." RTC_TSDR: " RTC_TSDR @ hex. cr
." RTC_TSSSR: " RTC_TSSSR @ hex. cr
." RTC_CALR: " RTC_CALR @ hex. cr
." RTC_TAFCR: " RTC_TAFCR @ hex. cr
." RTC_ALRMASSR: " RTC_ALRMASSR @ hex. cr
." RTC_ALRMBSSR: " RTC_ALRMBSSR @ hex. cr
." RTC_BKP0R: " RTC_BKP0R @ hex. cr
." RTC_BKP1R: " RTC_BKP1R @ hex. cr
." RTC_BKP2R: " RTC_BKP2R @ hex. cr
." RTC_BKP3R: " RTC_BKP3R @ hex. cr
." RTC_BKP4R: " RTC_BKP4R @ hex. cr
." RTC_BKP5R: " RTC_BKP5R @ hex. cr
." RTC_BKP6R: " RTC_BKP6R @ hex. cr
." RTC_BKP7R: " RTC_BKP7R @ hex. cr
." RTC_BKP8R: " RTC_BKP8R @ hex. cr
." RTC_BKP9R: " RTC_BKP9R @ hex. cr
." RTC_BKP10R: " RTC_BKP10R @ hex. cr
." RTC_BKP11R: " RTC_BKP11R @ hex. cr
." RTC_BKP12R: " RTC_BKP12R @ hex. cr
." RTC_BKP13R: " RTC_BKP13R @ hex. cr
." RTC_BKP14R: " RTC_BKP14R @ hex. cr
." RTC_BKP15R: " RTC_BKP15R @ hex. cr
." RTC_BKP16R: " RTC_BKP16R @ hex. cr
." RTC_BKP17R: " RTC_BKP17R @ hex. cr
." RTC_BKP18R: " RTC_BKP18R @ hex. cr
." RTC_BKP19R: " RTC_BKP19R @ hex. cr
;

: SDIO. cr
." SDIO_POWER: " SDIO_POWER @ hex. cr
." SDIO_CLKCR: " SDIO_CLKCR @ hex. cr
." SDIO_ARG: " SDIO_ARG @ hex. cr
." SDIO_CMD: " SDIO_CMD @ hex. cr
." SDIO_RESPCMD: " SDIO_RESPCMD @ hex. cr
." SDIO_RESP1: " SDIO_RESP1 @ hex. cr
." SDIO_RESP2: " SDIO_RESP2 @ hex. cr
." SDIO_RESP3: " SDIO_RESP3 @ hex. cr
." SDIO_RESP4: " SDIO_RESP4 @ hex. cr
." SDIO_DTIMER: " SDIO_DTIMER @ hex. cr
." SDIO_DLEN: " SDIO_DLEN @ hex. cr
." SDIO_DCTRL: " SDIO_DCTRL @ hex. cr
." SDIO_DCOUNT: " SDIO_DCOUNT @ hex. cr
." SDIO_STA: " SDIO_STA @ hex. cr
." SDIO_ICR: " SDIO_ICR @ hex. cr
." SDIO_MASK: " SDIO_MASK @ hex. cr
." SDIO_FIFOCNT: " SDIO_FIFOCNT @ hex. cr
." SDIO_FIFO: " SDIO_FIFO @ hex. cr
;

: SYSCFG. cr
." SYSCFG_MEMRM: " SYSCFG_MEMRM @ hex. cr
." SYSCFG_PMC: " SYSCFG_PMC @ hex. cr
." SYSCFG_EXTICR1: " SYSCFG_EXTICR1 @ hex. cr
." SYSCFG_EXTICR2: " SYSCFG_EXTICR2 @ hex. cr
." SYSCFG_EXTICR3: " SYSCFG_EXTICR3 @ hex. cr
." SYSCFG_EXTICR4: " SYSCFG_EXTICR4 @ hex. cr
." SYSCFG_CMPCR: " SYSCFG_CMPCR @ hex. cr
;

: TIM1. cr
." TIM1_CR1: " TIM1_CR1 @ hex. cr
." TIM1_CR2: " TIM1_CR2 @ hex. cr
." TIM1_SMCR: " TIM1_SMCR @ hex. cr
." TIM1_DIER: " TIM1_DIER @ hex. cr
." TIM1_SR: " TIM1_SR @ hex. cr
." TIM1_EGR: " TIM1_EGR @ hex. cr
." TIM1_CCMR1_Output: " TIM1_CCMR1_Output @ hex. cr
." TIM1_CCMR1_Input: " TIM1_CCMR1_Input @ hex. cr
." TIM1_CCMR2_Output: " TIM1_CCMR2_Output @ hex. cr
." TIM1_CCMR2_Input: " TIM1_CCMR2_Input @ hex. cr
." TIM1_CCER: " TIM1_CCER @ hex. cr
." TIM1_CNT: " TIM1_CNT @ hex. cr
." TIM1_PSC: " TIM1_PSC @ hex. cr
." TIM1_ARR: " TIM1_ARR @ hex. cr
." TIM1_CCR1: " TIM1_CCR1 @ hex. cr
." TIM1_CCR2: " TIM1_CCR2 @ hex. cr
." TIM1_CCR3: " TIM1_CCR3 @ hex. cr
." TIM1_CCR4: " TIM1_CCR4 @ hex. cr
." TIM1_DCR: " TIM1_DCR @ hex. cr
." TIM1_DMAR: " TIM1_DMAR @ hex. cr
." TIM1_RCR: " TIM1_RCR @ hex. cr
." TIM1_BDTR: " TIM1_BDTR @ hex. cr
;

: TIM8. cr
;

: TIM10. cr
." TIM10_CR1: " TIM10_CR1 @ hex. cr
." TIM10_DIER: " TIM10_DIER @ hex. cr
." TIM10_SR: " TIM10_SR @ hex. cr
." TIM10_EGR: " TIM10_EGR @ hex. cr
." TIM10_CCMR1_Output: " TIM10_CCMR1_Output @ hex. cr
." TIM10_CCMR1_Input: " TIM10_CCMR1_Input @ hex. cr
." TIM10_CCER: " TIM10_CCER @ hex. cr
." TIM10_CNT: " TIM10_CNT @ hex. cr
." TIM10_PSC: " TIM10_PSC @ hex. cr
." TIM10_ARR: " TIM10_ARR @ hex. cr
." TIM10_CCR1: " TIM10_CCR1 @ hex. cr
;

: TIM11. cr
." TIM11_CR1: " TIM11_CR1 @ hex. cr
." TIM11_DIER: " TIM11_DIER @ hex. cr
." TIM11_SR: " TIM11_SR @ hex. cr
." TIM11_EGR: " TIM11_EGR @ hex. cr
." TIM11_CCMR1_Output: " TIM11_CCMR1_Output @ hex. cr
." TIM11_CCMR1_Input: " TIM11_CCMR1_Input @ hex. cr
." TIM11_CCER: " TIM11_CCER @ hex. cr
." TIM11_CNT: " TIM11_CNT @ hex. cr
." TIM11_PSC: " TIM11_PSC @ hex. cr
." TIM11_ARR: " TIM11_ARR @ hex. cr
." TIM11_CCR1: " TIM11_CCR1 @ hex. cr
." TIM11_OR: " TIM11_OR @ hex. cr
;

: TIM2. cr
." TIM2_CR1: " TIM2_CR1 @ hex. cr
." TIM2_CR2: " TIM2_CR2 @ hex. cr
." TIM2_SMCR: " TIM2_SMCR @ hex. cr
." TIM2_DIER: " TIM2_DIER @ hex. cr
." TIM2_SR: " TIM2_SR @ hex. cr
." TIM2_EGR: " TIM2_EGR @ hex. cr
." TIM2_CCMR1_Output: " TIM2_CCMR1_Output @ hex. cr
." TIM2_CCMR1_Input: " TIM2_CCMR1_Input @ hex. cr
." TIM2_CCMR2_Output: " TIM2_CCMR2_Output @ hex. cr
." TIM2_CCMR2_Input: " TIM2_CCMR2_Input @ hex. cr
." TIM2_CCER: " TIM2_CCER @ hex. cr
." TIM2_CNT: " TIM2_CNT @ hex. cr
." TIM2_PSC: " TIM2_PSC @ hex. cr
." TIM2_ARR: " TIM2_ARR @ hex. cr
." TIM2_CCR1: " TIM2_CCR1 @ hex. cr
." TIM2_CCR2: " TIM2_CCR2 @ hex. cr
." TIM2_CCR3: " TIM2_CCR3 @ hex. cr
." TIM2_CCR4: " TIM2_CCR4 @ hex. cr
." TIM2_DCR: " TIM2_DCR @ hex. cr
." TIM2_DMAR: " TIM2_DMAR @ hex. cr
." TIM2_OR: " TIM2_OR @ hex. cr
;

: TIM3. cr
." TIM3_CR1: " TIM3_CR1 @ hex. cr
." TIM3_CR2: " TIM3_CR2 @ hex. cr
." TIM3_SMCR: " TIM3_SMCR @ hex. cr
." TIM3_DIER: " TIM3_DIER @ hex. cr
." TIM3_SR: " TIM3_SR @ hex. cr
." TIM3_EGR: " TIM3_EGR @ hex. cr
." TIM3_CCMR1_Output: " TIM3_CCMR1_Output @ hex. cr
." TIM3_CCMR1_Input: " TIM3_CCMR1_Input @ hex. cr
." TIM3_CCMR2_Output: " TIM3_CCMR2_Output @ hex. cr
." TIM3_CCMR2_Input: " TIM3_CCMR2_Input @ hex. cr
." TIM3_CCER: " TIM3_CCER @ hex. cr
." TIM3_CNT: " TIM3_CNT @ hex. cr
." TIM3_PSC: " TIM3_PSC @ hex. cr
." TIM3_ARR: " TIM3_ARR @ hex. cr
." TIM3_CCR1: " TIM3_CCR1 @ hex. cr
." TIM3_CCR2: " TIM3_CCR2 @ hex. cr
." TIM3_CCR3: " TIM3_CCR3 @ hex. cr
." TIM3_CCR4: " TIM3_CCR4 @ hex. cr
." TIM3_DCR: " TIM3_DCR @ hex. cr
." TIM3_DMAR: " TIM3_DMAR @ hex. cr
;

: TIM4. cr
;

: TIM5. cr
." TIM5_CR1: " TIM5_CR1 @ hex. cr
." TIM5_CR2: " TIM5_CR2 @ hex. cr
." TIM5_SMCR: " TIM5_SMCR @ hex. cr
." TIM5_DIER: " TIM5_DIER @ hex. cr
." TIM5_SR: " TIM5_SR @ hex. cr
." TIM5_EGR: " TIM5_EGR @ hex. cr
." TIM5_CCMR1_Output: " TIM5_CCMR1_Output @ hex. cr
." TIM5_CCMR1_Input: " TIM5_CCMR1_Input @ hex. cr
." TIM5_CCMR2_Output: " TIM5_CCMR2_Output @ hex. cr
." TIM5_CCMR2_Input: " TIM5_CCMR2_Input @ hex. cr
." TIM5_CCER: " TIM5_CCER @ hex. cr
." TIM5_CNT: " TIM5_CNT @ hex. cr
." TIM5_PSC: " TIM5_PSC @ hex. cr
." TIM5_ARR: " TIM5_ARR @ hex. cr
." TIM5_CCR1: " TIM5_CCR1 @ hex. cr
." TIM5_CCR2: " TIM5_CCR2 @ hex. cr
." TIM5_CCR3: " TIM5_CCR3 @ hex. cr
." TIM5_CCR4: " TIM5_CCR4 @ hex. cr
." TIM5_DCR: " TIM5_DCR @ hex. cr
." TIM5_DMAR: " TIM5_DMAR @ hex. cr
." TIM5_OR: " TIM5_OR @ hex. cr
;

: TIM9. cr
." TIM9_CR1: " TIM9_CR1 @ hex. cr
." TIM9_CR2: " TIM9_CR2 @ hex. cr
." TIM9_SMCR: " TIM9_SMCR @ hex. cr
." TIM9_DIER: " TIM9_DIER @ hex. cr
." TIM9_SR: " TIM9_SR @ hex. cr
." TIM9_EGR: " TIM9_EGR @ hex. cr
." TIM9_CCMR1_Output: " TIM9_CCMR1_Output @ hex. cr
." TIM9_CCMR1_Input: " TIM9_CCMR1_Input @ hex. cr
." TIM9_CCER: " TIM9_CCER @ hex. cr
." TIM9_CNT: " TIM9_CNT @ hex. cr
." TIM9_PSC: " TIM9_PSC @ hex. cr
." TIM9_ARR: " TIM9_ARR @ hex. cr
." TIM9_CCR1: " TIM9_CCR1 @ hex. cr
." TIM9_CCR2: " TIM9_CCR2 @ hex. cr
;

: USART1. cr
." USART1_SR: " USART1_SR @ hex. cr
." USART1_DR: " USART1_DR @ hex. cr
." USART1_BRR: " USART1_BRR @ hex. cr
." USART1_CR1: " USART1_CR1 @ hex. cr
." USART1_CR2: " USART1_CR2 @ hex. cr
." USART1_CR3: " USART1_CR3 @ hex. cr
." USART1_GTPR: " USART1_GTPR @ hex. cr
;

: USART2. cr
;

: USART6. cr
;

: WWDG. cr
." WWDG_CR: " WWDG_CR @ hex. cr
." WWDG_CFR: " WWDG_CFR @ hex. cr
." WWDG_SR: " WWDG_SR @ hex. cr
;

: DMA2. cr
." DMA2_LISR: " DMA2_LISR @ hex. cr
." DMA2_HISR: " DMA2_HISR @ hex. cr
." DMA2_LIFCR: " DMA2_LIFCR @ hex. cr
." DMA2_HIFCR: " DMA2_HIFCR @ hex. cr
." DMA2_S0CR: " DMA2_S0CR @ hex. cr
." DMA2_S0NDTR: " DMA2_S0NDTR @ hex. cr
." DMA2_S0PAR: " DMA2_S0PAR @ hex. cr
." DMA2_S0M0AR: " DMA2_S0M0AR @ hex. cr
." DMA2_S0M1AR: " DMA2_S0M1AR @ hex. cr
." DMA2_S0FCR: " DMA2_S0FCR @ hex. cr
." DMA2_S1CR: " DMA2_S1CR @ hex. cr
." DMA2_S1NDTR: " DMA2_S1NDTR @ hex. cr
." DMA2_S1PAR: " DMA2_S1PAR @ hex. cr
." DMA2_S1M0AR: " DMA2_S1M0AR @ hex. cr
." DMA2_S1M1AR: " DMA2_S1M1AR @ hex. cr
." DMA2_S1FCR: " DMA2_S1FCR @ hex. cr
." DMA2_S2CR: " DMA2_S2CR @ hex. cr
." DMA2_S2NDTR: " DMA2_S2NDTR @ hex. cr
." DMA2_S2PAR: " DMA2_S2PAR @ hex. cr
." DMA2_S2M0AR: " DMA2_S2M0AR @ hex. cr
." DMA2_S2M1AR: " DMA2_S2M1AR @ hex. cr
." DMA2_S2FCR: " DMA2_S2FCR @ hex. cr
." DMA2_S3CR: " DMA2_S3CR @ hex. cr
." DMA2_S3NDTR: " DMA2_S3NDTR @ hex. cr
." DMA2_S3PAR: " DMA2_S3PAR @ hex. cr
." DMA2_S3M0AR: " DMA2_S3M0AR @ hex. cr
." DMA2_S3M1AR: " DMA2_S3M1AR @ hex. cr
." DMA2_S3FCR: " DMA2_S3FCR @ hex. cr
." DMA2_S4CR: " DMA2_S4CR @ hex. cr
." DMA2_S4NDTR: " DMA2_S4NDTR @ hex. cr
." DMA2_S4PAR: " DMA2_S4PAR @ hex. cr
." DMA2_S4M0AR: " DMA2_S4M0AR @ hex. cr
." DMA2_S4M1AR: " DMA2_S4M1AR @ hex. cr
." DMA2_S4FCR: " DMA2_S4FCR @ hex. cr
." DMA2_S5CR: " DMA2_S5CR @ hex. cr
." DMA2_S5NDTR: " DMA2_S5NDTR @ hex. cr
." DMA2_S5PAR: " DMA2_S5PAR @ hex. cr
." DMA2_S5M0AR: " DMA2_S5M0AR @ hex. cr
." DMA2_S5M1AR: " DMA2_S5M1AR @ hex. cr
." DMA2_S5FCR: " DMA2_S5FCR @ hex. cr
." DMA2_S6CR: " DMA2_S6CR @ hex. cr
." DMA2_S6NDTR: " DMA2_S6NDTR @ hex. cr
." DMA2_S6PAR: " DMA2_S6PAR @ hex. cr
." DMA2_S6M0AR: " DMA2_S6M0AR @ hex. cr
." DMA2_S6M1AR: " DMA2_S6M1AR @ hex. cr
." DMA2_S6FCR: " DMA2_S6FCR @ hex. cr
." DMA2_S7CR: " DMA2_S7CR @ hex. cr
." DMA2_S7NDTR: " DMA2_S7NDTR @ hex. cr
." DMA2_S7PAR: " DMA2_S7PAR @ hex. cr
." DMA2_S7M0AR: " DMA2_S7M0AR @ hex. cr
." DMA2_S7M1AR: " DMA2_S7M1AR @ hex. cr
." DMA2_S7FCR: " DMA2_S7FCR @ hex. cr
;

: DMA1. cr
;

: GPIOH. cr
." GPIOH_MODER: " GPIOH_MODER @ hex. cr
." GPIOH_OTYPER: " GPIOH_OTYPER @ hex. cr
." GPIOH_OSPEEDR: " GPIOH_OSPEEDR @ hex. cr
." GPIOH_PUPDR: " GPIOH_PUPDR @ hex. cr
." GPIOH_IDR: " GPIOH_IDR @ hex. cr
." GPIOH_ODR: " GPIOH_ODR @ hex. cr
." GPIOH_BSRR: " GPIOH_BSRR @ hex. cr
." GPIOH_LCKR: " GPIOH_LCKR @ hex. cr
." GPIOH_AFRL: " GPIOH_AFRL @ hex. cr
." GPIOH_AFRH: " GPIOH_AFRH @ hex. cr
;

: GPIOE. cr
;

: GPIOD. cr
;

: GPIOC. cr
;

: GPIOB. cr
." GPIOB_MODER: " GPIOB_MODER @ hex. cr
." GPIOB_OTYPER: " GPIOB_OTYPER @ hex. cr
." GPIOB_OSPEEDR: " GPIOB_OSPEEDR @ hex. cr
." GPIOB_PUPDR: " GPIOB_PUPDR @ hex. cr
." GPIOB_IDR: " GPIOB_IDR @ hex. cr
." GPIOB_ODR: " GPIOB_ODR @ hex. cr
." GPIOB_BSRR: " GPIOB_BSRR @ hex. cr
." GPIOB_LCKR: " GPIOB_LCKR @ hex. cr
." GPIOB_AFRL: " GPIOB_AFRL @ hex. cr
." GPIOB_AFRH: " GPIOB_AFRH @ hex. cr
;

: GPIOA. cr
." GPIOA_MODER: " GPIOA_MODER @ hex. cr
." GPIOA_OTYPER: " GPIOA_OTYPER @ hex. cr
." GPIOA_OSPEEDR: " GPIOA_OSPEEDR @ hex. cr
." GPIOA_PUPDR: " GPIOA_PUPDR @ hex. cr
." GPIOA_IDR: " GPIOA_IDR @ hex. cr
." GPIOA_ODR: " GPIOA_ODR @ hex. cr
." GPIOA_BSRR: " GPIOA_BSRR @ hex. cr
." GPIOA_LCKR: " GPIOA_LCKR @ hex. cr
." GPIOA_AFRL: " GPIOA_AFRL @ hex. cr
." GPIOA_AFRH: " GPIOA_AFRH @ hex. cr
;

: I2C3. cr
." I2C3_CR1: " I2C3_CR1 @ hex. cr
." I2C3_CR2: " I2C3_CR2 @ hex. cr
." I2C3_OAR1: " I2C3_OAR1 @ hex. cr
." I2C3_OAR2: " I2C3_OAR2 @ hex. cr
." I2C3_DR: " I2C3_DR @ hex. cr
." I2C3_SR1: " I2C3_SR1 @ hex. cr
." I2C3_SR2: " I2C3_SR2 @ hex. cr
." I2C3_CCR: " I2C3_CCR @ hex. cr
." I2C3_TRISE: " I2C3_TRISE @ hex. cr
;

: I2C2. cr
;

: I2C1. cr
;

: I2S2ext. cr
." I2S2ext_CR1: " I2S2ext_CR1 @ hex. cr
." I2S2ext_CR2: " I2S2ext_CR2 @ hex. cr
." I2S2ext_SR: " I2S2ext_SR @ hex. cr
." I2S2ext_DR: " I2S2ext_DR @ hex. cr
." I2S2ext_CRCPR: " I2S2ext_CRCPR @ hex. cr
." I2S2ext_RXCRCR: " I2S2ext_RXCRCR @ hex. cr
." I2S2ext_TXCRCR: " I2S2ext_TXCRCR @ hex. cr
." I2S2ext_I2SCFGR: " I2S2ext_I2SCFGR @ hex. cr
." I2S2ext_I2SPR: " I2S2ext_I2SPR @ hex. cr
;

: I2S3ext. cr
;

: SPI1. cr
;

: SPI2. cr
;

: SPI3. cr
;

: SPI4. cr
;

: SPI5. cr
;

: NVIC. cr
." NVIC_ICTR: " NVIC_ICTR @ hex. cr
." NVIC_STIR: " NVIC_STIR @ hex. cr
." NVIC_ISER0: " NVIC_ISER0 @ hex. cr
." NVIC_ISER1: " NVIC_ISER1 @ hex. cr
." NVIC_ISER2: " NVIC_ISER2 @ hex. cr
." NVIC_ICER0: " NVIC_ICER0 @ hex. cr
." NVIC_ICER1: " NVIC_ICER1 @ hex. cr
." NVIC_ICER2: " NVIC_ICER2 @ hex. cr
." NVIC_ISPR0: " NVIC_ISPR0 @ hex. cr
." NVIC_ISPR1: " NVIC_ISPR1 @ hex. cr
." NVIC_ISPR2: " NVIC_ISPR2 @ hex. cr
." NVIC_ICPR0: " NVIC_ICPR0 @ hex. cr
." NVIC_ICPR1: " NVIC_ICPR1 @ hex. cr
." NVIC_ICPR2: " NVIC_ICPR2 @ hex. cr
." NVIC_IABR0: " NVIC_IABR0 @ hex. cr
." NVIC_IABR1: " NVIC_IABR1 @ hex. cr
." NVIC_IABR2: " NVIC_IABR2 @ hex. cr
." NVIC_IPR0: " NVIC_IPR0 @ hex. cr
." NVIC_IPR1: " NVIC_IPR1 @ hex. cr
." NVIC_IPR2: " NVIC_IPR2 @ hex. cr
." NVIC_IPR3: " NVIC_IPR3 @ hex. cr
." NVIC_IPR4: " NVIC_IPR4 @ hex. cr
." NVIC_IPR5: " NVIC_IPR5 @ hex. cr
." NVIC_IPR6: " NVIC_IPR6 @ hex. cr
." NVIC_IPR7: " NVIC_IPR7 @ hex. cr
." NVIC_IPR8: " NVIC_IPR8 @ hex. cr
." NVIC_IPR9: " NVIC_IPR9 @ hex. cr
." NVIC_IPR10: " NVIC_IPR10 @ hex. cr
." NVIC_IPR11: " NVIC_IPR11 @ hex. cr
." NVIC_IPR12: " NVIC_IPR12 @ hex. cr
." NVIC_IPR13: " NVIC_IPR13 @ hex. cr
." NVIC_IPR14: " NVIC_IPR14 @ hex. cr
." NVIC_IPR15: " NVIC_IPR15 @ hex. cr
." NVIC_IPR16: " NVIC_IPR16 @ hex. cr
." NVIC_IPR17: " NVIC_IPR17 @ hex. cr
." NVIC_IPR18: " NVIC_IPR18 @ hex. cr
." NVIC_IPR19: " NVIC_IPR19 @ hex. cr
;

